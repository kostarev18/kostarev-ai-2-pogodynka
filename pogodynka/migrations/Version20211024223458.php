<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211024223458 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE measurements (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, location_id_id INTEGER NOT NULL, day_timestamp DATE NOT NULL, temperature NUMERIC(5, 2) NOT NULL, humidity NUMERIC(5, 2) NOT NULL, pressure NUMERIC(5, 2) NOT NULL, feels_like_temp NUMERIC(5, 2) NOT NULL, min_temp NUMERIC(5, 2) NOT NULL, max_temp NUMERIC(5, 2) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_71920F21918DB72 ON measurements (location_id_id)');
        $this->addSql('DROP TABLE weather_forecast');
        $this->addSql('DROP TABLE weather_forecast_location');
        $this->addSql('DROP INDEX IDX_2D5B0234918DB72');
        $this->addSql('CREATE TEMPORARY TABLE __temp__city AS SELECT id, location_id_id, city_name, zipcode, longtitude, latitude FROM city');
        $this->addSql('DROP TABLE city');
        $this->addSql('CREATE TABLE city (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, location_id_id INTEGER NOT NULL, city_name VARCHAR(200) NOT NULL, zipcode VARCHAR(200) NOT NULL, longtitude NUMERIC(10, 7) NOT NULL, latitude NUMERIC(10, 7) NOT NULL, CONSTRAINT FK_2D5B0234918DB72 FOREIGN KEY (location_id_id) REFERENCES location (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO city (id, location_id_id, city_name, zipcode, longtitude, latitude) SELECT id, location_id_id, city_name, zipcode, longtitude, latitude FROM __temp__city');
        $this->addSql('DROP TABLE __temp__city');
        $this->addSql('CREATE INDEX IDX_2D5B0234918DB72 ON city (location_id_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__location AS SELECT id, country, region FROM location');
        $this->addSql('DROP TABLE location');
        $this->addSql('CREATE TABLE location (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, country VARCHAR(200) NOT NULL, region VARCHAR(100) NOT NULL)');
        $this->addSql('INSERT INTO location (id, country, region) SELECT id, country, region FROM __temp__location');
        $this->addSql('DROP TABLE __temp__location');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE weather_forecast (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, day_timestamp DATE NOT NULL, temperature VARCHAR(10) NOT NULL COLLATE BINARY, humidity NUMERIC(10, 3) NOT NULL, pressure NUMERIC(10, 2) NOT NULL, feels_like_temp VARCHAR(10) NOT NULL COLLATE BINARY, min_temp VARCHAR(10) NOT NULL COLLATE BINARY, max_temp VARCHAR(10) NOT NULL COLLATE BINARY)');
        $this->addSql('CREATE TABLE weather_forecast_location (weather_forecast_id INTEGER NOT NULL, location_id INTEGER NOT NULL, PRIMARY KEY(weather_forecast_id, location_id))');
        $this->addSql('CREATE INDEX IDX_8822D94864D218E ON weather_forecast_location (location_id)');
        $this->addSql('CREATE INDEX IDX_8822D9486159309F ON weather_forecast_location (weather_forecast_id)');
        $this->addSql('DROP TABLE measurements');
        $this->addSql('DROP INDEX IDX_2D5B0234918DB72');
        $this->addSql('CREATE TEMPORARY TABLE __temp__city AS SELECT id, location_id_id, city_name, zipcode, longtitude, latitude FROM city');
        $this->addSql('DROP TABLE city');
        $this->addSql('CREATE TABLE city (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, location_id_id INTEGER NOT NULL, city_name VARCHAR(255) NOT NULL COLLATE BINARY, zipcode VARCHAR(50) NOT NULL COLLATE BINARY, longtitude NUMERIC(10, 7) NOT NULL, latitude NUMERIC(10, 7) NOT NULL)');
        $this->addSql('INSERT INTO city (id, location_id_id, city_name, zipcode, longtitude, latitude) SELECT id, location_id_id, city_name, zipcode, longtitude, latitude FROM __temp__city');
        $this->addSql('DROP TABLE __temp__city');
        $this->addSql('CREATE INDEX IDX_2D5B0234918DB72 ON city (location_id_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__location AS SELECT id, country, region FROM location');
        $this->addSql('DROP TABLE location');
        $this->addSql('CREATE TABLE location (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, country VARCHAR(50) NOT NULL COLLATE BINARY, region VARCHAR(50) NOT NULL COLLATE BINARY)');
        $this->addSql('INSERT INTO location (id, country, region) SELECT id, country, region FROM __temp__location');
        $this->addSql('DROP TABLE __temp__location');
    }
}
