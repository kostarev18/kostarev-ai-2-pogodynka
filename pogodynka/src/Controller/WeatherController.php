<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\City;
use App\Repository\MeasurementRepository;
use App\Repository\CityRepository;


class WeatherController extends AbstractController
{
    public function cityAction(City $city, MeasurementRepository $measurementRepository): Response
    {
        $measurements = $measurementRepository->findByLocation($city);

        return $this->render('weather/city.html.twig', [
            'location' => $city,
            'measurements' => $measurements,
        ]);
    }

    public function cityActionModified(City $city, MeasurementRepository $measurementRepository): Response
    {
        $measurements = $measurementRepository->findByLocationTomorrow($city);

        return $this->render('weather/tomorrow.html.twig', [
            'location' => $city,
            'measurements' => $measurements,
        ]);
    }

    public function countryCity(string $country, string $city, MeasurementRepository $measurementRepository, CityRepository $cityRepository): Response
    {
        $miasto = $cityRepository->findOneBy(['name'=>$city, 'country'=>$country]);

        if($miasto == null){
            return $this->render('weather/page_404.html.twig', [
                'country' => $country,
                'city' => $city,
            ]);
        }

        $measurements = $measurementRepository->findByLocation($miasto);

        return $this->render('weather/city.html.twig', [
            'location' => $miasto,
            'measurements' => $measurements,
        ]);
    }

}
