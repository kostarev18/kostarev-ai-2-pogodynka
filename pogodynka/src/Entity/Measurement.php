<?php

namespace App\Entity;

use App\Repository\MeasurementRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MeasurementRepository::class)
 */
class Measurement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=City::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $city_id;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $temperature;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $humidity;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $pressure;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $feels_like_temp;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2, nullable=true)
     */
    private $min_temp;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2, nullable=true)
     */
    private $max_temp;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCityId(): ?City
    {
        return $this->city_id;
    }

    public function setCityId(?City $city_id): self
    {
        $this->city_id = $city_id;

        return $this;
    }

    public function getTemperature(): ?string
    {
        return $this->temperature;
    }

    public function setTemperature(string $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    public function getHumidity(): ?string
    {
        return $this->humidity;
    }

    public function setHumidity(string $humidity): self
    {
        $this->humidity = $humidity;

        return $this;
    }

    public function getPressure(): ?string
    {
        return $this->pressure;
    }

    public function setPressure(string $pressure): self
    {
        $this->pressure = $pressure;

        return $this;
    }

    public function getFeelsLikeTemp(): ?string
    {
        return $this->feels_like_temp;
    }

    public function setFeelsLikeTemp(string $feels_like_temp): self
    {
        $this->feels_like_temp = $feels_like_temp;

        return $this;
    }

    public function getMinTemp(): ?string
    {
        return $this->min_temp;
    }

    public function setMinTemp(?string $min_temp): self
    {
        $this->min_temp = $min_temp;

        return $this;
    }

    public function getMaxTemp(): ?string
    {
        return $this->max_temp;
    }

    public function setMaxTemp(?string $max_temp): self
    {
        $this->max_temp = $max_temp;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
