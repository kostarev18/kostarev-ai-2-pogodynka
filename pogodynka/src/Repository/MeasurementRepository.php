<?php 

namespace App\Repository;

use App\Entity\Measurement;
use App\Entity\City;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class MeasurementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Measurement::class);
    }

    public function findByLocation(City $city)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->where('m.city_id = :city_id')
            ->setParameter('city_id', $city->getId())
            ->andWhere('m.date <= :now')
            ->setParameter('now', date('Y-m-d'));

        $query = $qb->getQuery();
        $result = $query->getResult();
        return $result;
    }

    public function findByLocationTomorrow(City $city)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->where('m.city_id = :city_id')
            ->setParameter('city_id', $city->getId())
            ->andWhere('m.date > :now')
            ->setParameter('now', date('Y-m-d'))
            ->orderBy('m.temperature','asc');

        $query = $qb->getQuery();
        $result = $query->getResult();
        return $result;
    }
}

